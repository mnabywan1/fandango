echo "-----------------------------------------------------------------------"
echo "PYTEST"
echo ""

pytest-3 .

echo ""
echo "-----------------------------------------------------------------------"
echo "COMPILING"
echo ""

python3 -m compileall fandango

echo ""
echo "-----------------------------------------------------------------------"
echo "PYFLAKES"
echo ""

pyflakes3 fandango | grep -v "unable\ to\ detect\ undefined\ names" | grep -v "unused" | grep -v "star\ imports" | grep -v "never\ used"
fandango/scripts/fandango -v

echo ""
echo "-----------------------------------------------------------------------"
echo ""

read -p 'Do you want to remove previous packages and upload latest? (y/n)' confirm

if [ "$(echo $confirm | grep 'y\|Y')" ] ; then
    python3 -m build --sdist
    python3 -m build --wheel
    twine check dist/*
    twine upload dist/*
fi

